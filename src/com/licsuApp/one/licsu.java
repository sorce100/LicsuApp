package com.licsuApp.one;

import com.codename1.capture.Capture;
import com.codename1.components.ClearableTextField;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.ScaleImageLabel;
import com.codename1.io.FileSystemStorage;
import com.codename1.io.JSONParser;
import static com.codename1.ui.CN.*;
import com.codename1.ui.Form;
import com.codename1.ui.Dialog;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.io.Log;
import com.codename1.io.Storage;
import com.codename1.io.Util;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.location.Location;
import com.codename1.location.LocationManager;
import com.codename1.processing.Result;
import com.codename1.ui.AutoCompleteTextField;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.spinner.Picker;
import java.io.IOException;
import java.util.HashMap;
import java.lang.Object;
import java.util.Map;
import java.util.Vector;
import org.littlemonkey.connectivity.Connectivity;

public class licsu {

    private Form current;
    private Resources theme;

    public void init(Object context) {
        // use two network threads instead of one
        updateNetworkThreadCount(2);

        theme = UIManager.initFirstTheme("/theme");

        // Enable Toolbar on all Forms by default
        Toolbar.setGlobalToolbar(true);

        // Pro only feature
        Log.bindCrashProtection(true);

        addNetworkErrorListener(err -> {
            // prevent the event from propagating
            err.consume();
            if(err.getError() != null) {
                Log.e(err.getError());
            }
            Log.sendLogAsync();
            Dialog.show("Connection Error", "There was a networking error in the connection to " + err.getConnectionRequest().getUrl(), "OK", null);
        }); 
        
         // Enable the "Hamburger" menu
    }
    public void start() {
        Display.getInstance().setProperty("blockCopyPaste", "true");
        if(current != null){
            current.show();
            return;
        }
        showlogin();
//        showdatalist();
    }
    public void stop() {
        current = getCurrentForm();
        if(current instanceof Dialog) {
            ((Dialog)current).dispose();
            current = getCurrentForm();
        }
    }
//    error message
    public void showError(String msg) {
        Dialog.show("Failed", msg, "OK", null);
    }
    public void destroy() {
    }
    
//    method for creating and showing the login form
    public void showlogin(){
//        checking if data is connected or not
        try{
            if (Connectivity.isConnected()) {
                
             } else {
            Dialog.show("WARNING", "No connection,Please Turn on your Internet to continue", "OK",null);
            } 
        
        }catch (Exception ex) {
                return;
            }
        
//        start of form
        Form f = new Form("LICSU FIELD APP");
        Container padding = new Container();
        Style s = new Style();
        s.setPadding(0, 15, 5, 5);
        s.setPaddingUnit(new byte[]{
            Style.UNIT_TYPE_DIPS, 
            Style.UNIT_TYPE_DIPS,
            Style.UNIT_TYPE_DIPS,
            Style.UNIT_TYPE_DIPS
        });
        
        padding.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        Label logo = new Label();
        padding.add(logo);
        Image img = theme.getImage("gwlogo.png");
        ScaleImageLabel btn = new ScaleImageLabel(img);
        btn.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FIT);
        padding.add(btn);

//        PickerComponent date = PickerComponent.createDate(new Date()).label("Date");
//        TextComponent title = new TextComponent().label("Title"); 
//          Display.getInstance().sendSMS("+233209969656", "This is a test App");
        padding.addComponent(new Label("Username"));
        TextField usernameField = new TextField();
        TextField passwordField = new TextField();
        passwordField.setConstraint(TextField.PASSWORD);
        padding.addComponent(usernameField);
        padding.addComponent(new Label("Password"));
        padding.addComponent(passwordField);
        
        Button loginButton = new Button("LOGIN","RaisedButton");
        FontImage.setMaterialIcon(loginButton, FontImage.MATERIAL_LOCK_OPEN);
        loginButton.addActionListener((e)->{
            try {
                
                
                String username = usernameField.getText().trim();
//                storing the username
               Vector<String> usernameref = new Vector<>();
               usernameref.addElement(username);
               Storage.getInstance().writeObject("usernameref", usernameref);
//            password
                String password = passwordField.getText().trim();
//                the spinner
                Dialog spinner = new InfiniteProgress().showInifiniteBlocking();
                
                if(username.length()== 0 & password.length() == 0){
                    spinner.dispose();
                Dialog.show("LOGIN FAILED", "Username or Password is Empty", "OK", null);
                }else{
                    
                  Map<String, String> logindata = new HashMap<String,String>();
                  logindata.put("username", username);
                  logindata.put("password", password);
                  final String result = Result.fromContent(logindata).toString(); 
                  
                    String url = "http://139.162.226.4/api/public/index.php/api/officers/login";
        
                Response<Map> jsonData = Rest.post(url).contentType("application/json").body(result).acceptJson().getAsJsonMap();
                int jsonresponse = jsonData.getResponseCode();
                 JSONParser parser = new JSONParser();
                 if(jsonresponse == 200){
                     spinner.dispose();
                     showdatalist();
                 }
                 if(jsonresponse == 404){
                     spinner.dispose();
                 Dialog.show("Login Failed", "Incorrect Username or Password", "OK",null);
                 }
                }
            }
            catch (Exception ex) {
                Log.e(ex);
                Dialog.show("Login Failed", ex.getMessage(), "OK",null);
                return;
            }
        });

        padding.addComponent(loginButton);
        
        Button fclose = new Button("CLOSE","RaisedButton");
        FontImage.setMaterialIcon(fclose, FontImage.MATERIAL_CLOSE);
        fclose.addActionListener((e) -> {
           System.exit(0); 
        });
        
        padding.addComponent(fclose);
        f.setLayout(new BorderLayout());
        f.addComponent(BorderLayout.CENTER, padding);
        f.show();
    }
//    for datalist
    public void showdatalist(){
        
        if (LocationManager.getLocationManager().isGPSEnabled()) {    
                }else{
                    Dialog.show("WARNING", "Please Turn on Your GPS", "OK",null);
                }
        
//        retrieving dropdown logo
            Image img = theme.getImage("combo.png");
//        retrieving username from storage
            Vector<String> sidemenuusername =  (Vector<String>)Storage.getInstance().readObject("usernameref");
            String sidemenuusernamevalue = sidemenuusername.elementAt(0);
//    for the side menu 
    Form f = new Form("APPLICANT DETAILS");
     Toolbar tb = f.getToolbar();
       Container topBar = BorderLayout.center(new Label("WELCOME"+" "+sidemenuusernamevalue.toUpperCase()));
        topBar.add(BorderLayout.SOUTH, new Label("MENU", "SidemenuTagline")); 
        topBar.setUIID("SideCommand");
        tb.addComponentToSideMenu(topBar);

        tb.addMaterialCommandToSideMenu("New", FontImage.MATERIAL_HOME, e -> {showdatalist();}); 
        tb.addMaterialCommandToSideMenu("Change Password", FontImage.MATERIAL_SETTINGS, e -> {showchangepass();});
//        getting the current page
//        final Form currentForm = Display.getInstance().getCurrent();
        
        tb.addMaterialCommandToSideMenu("Logout", FontImage.MATERIAL_HOURGLASS_FULL, e -> {showlogin();});
 
        Container padding = new Container();
        padding.setScrollableY(true);
        Style s = new Style();
        s.setPadding(0, 0, 0, 0);
        s.setPaddingUnit(new byte[]{
            Style.UNIT_TYPE_DIPS, 
            Style.UNIT_TYPE_DIPS,
            Style.UNIT_TYPE_DIPS,
            Style.UNIT_TYPE_DIPS
        });    
        padding.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
//        for district
            padding.addComponent(new Label("GWCL District")); 
            AutoCompleteTextField districtlist = new AutoCompleteTextField("","Accra Metropolitan District","Ada East District","Ada West District","Adenta Municipal District","Ashaiman Municipal District","Ga Central District","Ga East Municipal District");
//            String[] districtlist = {"","Accra Metropolitan District", "Ada East District", "Adenta Municipal District", "Ashaiman Municipal District","Ga Central District"};
//            Picker districtpick = new Picker();
//            districtpick.setIcon(img);
//            districtpick.setStrings(districtlist);
//            districtpick.setSelectedString(districtlist[0]);
            padding.add(districtlist);
            
//          for community name
        padding.addComponent(new Label("Community Name"));
        TextField communitynameField = new TextField();
        padding.addComponent(ClearableTextField.wrap(communitynameField));
         
//        for Beneficiary contact
         padding.addComponent(new Label("Beneficiary Contact"));
        TextField beneficiarynameField = new TextField();
        beneficiarynameField.setConstraint(TextField.NUMERIC);
         padding.addComponent(ClearableTextField.wrap(beneficiarynameField)); 
         
//        for sex of Applicant
            padding.addComponent(new Label("Sex of Applicant"));    
            String[] sexlist = {"", "Male", "Female", "Couple"};
            Picker sexpick = new Picker();
            sexpick.setIcon(img);
            sexpick.setStrings(sexlist);
            sexpick.setSelectedString(sexlist[0]);
            padding.add(sexpick);
                   
//        Number of Househod in the house
            padding.addComponent(new Label("Number of household in the house"));
        TextField numofhouseholdField = new TextField();
        numofhouseholdField.setConstraint(TextField.NUMERIC);
         padding.addComponent(ClearableTextField.wrap(numofhouseholdField));
                 
//         number of household in single rooms
        padding.addComponent(new Label("Number of Single rooms"));
        TextField numofsingleroomField = new TextField();
        numofsingleroomField.setConstraint(TextField.NUMERIC);
         padding.addComponent(ClearableTextField.wrap(numofsingleroomField));      
         
//        Number of Males 
         padding.addComponent(new Label("Number of Males in the house"));
        TextField numofmalesField = new TextField();
        numofmalesField.setConstraint(TextField.NUMERIC);
         padding.addComponent(ClearableTextField.wrap(numofmalesField));       
         
//         Number of Females
        padding.addComponent(new Label("Number of Females in the house"));
        TextField numoffemalesField = new TextField();
        numoffemalesField.setConstraint(TextField.NUMERIC);
         padding.addComponent(ClearableTextField.wrap(numoffemalesField));
         
//         type of intervention
            padding.addComponent(new Label("Type of Intervention"));    
            String[] interventionlist = {"", "Intervention1", "Intervention 2", "Intervention 3"};
            Picker interventionpick = new Picker();
             interventionpick.setIcon(img);
            interventionpick.setStrings(interventionlist);
            interventionpick.setSelectedString(interventionlist[0]);
            padding.add(interventionpick);
             
//         GPS location of Meter 
//Check if location is turned on and your app is allowed to use it.
//    if (LocationManager.getLocationManager().isGPSEnabled()) {
          Location position = LocationManager.getLocationManager().getCurrentLocationSync();  
        padding.addComponent(new Label("GPS location of Meter"));
        double gpscordlatitude = position.getLatitude();
        double gpscordlongitude = position.getLongitude();
         TextField gpscord = new TextField(gpscordlatitude +"  "+gpscordlongitude);
        gpscord.setEditable(false);
        padding.add(gpscord);

//for picture of meter
         padding.addComponent(new Label("Picture of Meter"));
         Button meterpicture = new Button("Attach Photo");
        meterpicture.setTextPosition(Label.BOTTOM);
        padding.addComponent(meterpicture);
        
        meterpicture.addActionListener((evt)-> {
            String file = Capture.capturePhoto(1024, -1);
            if (file == null) {
                return;
            }
            try {
                 Util.copy(FileSystemStorage.getInstance().openInputStream(file), Storage.getInstance().createOutputStream(beneficiarynameField.getText())); 
               Vector<String> meterpic = new Vector<>();
               meterpic.addElement(file);
               Storage.getInstance().writeObject("meterpic", meterpic);
               
                Image meterimg = Image.createImage(file).scaledSmallerRatio(256, 256);
                meterpicture.setIcon(meterimg);
                f.revalidate();
            } catch (IOException ex) {
                showError(ex.getMessage());
                return;
            }

        });
//      for picture of beneficiary pipe / houser
        padding.addComponent(new Label("Picture of Beneficiary Pipe/ houser"));
         Button pipepicture = new Button("Attach Photo");
        pipepicture.setTextPosition(Label.BOTTOM);
        padding.addComponent(pipepicture);
        
        pipepicture.addActionListener((evt)-> {
            String file = Capture.capturePhoto(1024, -1);
            if (file == null) {
                return;
            }
            try {
               Vector<String> pipepic = new Vector<>();
               pipepic.addElement(file);
               Storage.getInstance().writeObject("pipepic", pipepic);
                       
                Image pipeimg = Image.createImage(file).scaledSmallerRatio(256, 256);
                pipepicture.setIcon(pipeimg);
                f.revalidate();
            } catch (IOException ex) {
                showError(ex.getMessage());
                return;
            }

        });
//for additional comments
        padding.addComponent(new Label("Additional Comment (If Any)"));
        TextArea commentField = new TextArea();
        commentField.setRows(5);
        commentField.setHint("Enter any additional");
        padding.add(commentField);
           
        Button submitButton = new Button("SUBMIT"); 
        submitButton.addActionListener((evt)-> { 
//            for district
        String districtpickvalue= districtlist.getText().trim().toUpperCase();
//            for community name
         String communitynamevalue = communitynameField.getText().trim().toUpperCase();
//         for beneficiary
         String beneficiarynamevalue = beneficiarynameField.getText().trim().toUpperCase();
//         sex 
         String sexpickvalue= sexpick.getSelectedString().trim().toUpperCase();
//         number of household
          String numberhouseholdvalue= numofhouseholdField.getText().trim().toUpperCase();
//          number of single room
        String numberofsingleroomvalue= numofsingleroomField.getText().trim().toUpperCase();
//        number of males
          String numberofmalesvalue= numofmalesField.getText().trim().toUpperCase();
//          number of females
        String numberoffemalesvalue= numoffemalesField.getText().trim().toUpperCase();
//        intervention
            String interventionpickvalue= interventionpick.getSelectedString().trim().toUpperCase();
//          gps cord
        String gpscordvalue= gpscord.getText().trim().toUpperCase();
//        meter image
            Vector<String> meterpic =  (Vector<String>)Storage.getInstance().readObject("meterpic");
            String meterpicvalue = meterpic.elementAt(0);
//          retrieving pipepicture image from storage 
            Vector<String> pipepic =  (Vector<String>)Storage.getInstance().readObject("pipepic");
            String pipepicvalue = pipepic.elementAt(0);
//          comment field
        String commentFieldvalue = commentField.getText().trim().toUpperCase();
//        retrieving username
        Vector<String> usernameref =  (Vector<String>)Storage.getInstance().readObject("usernameref");
            String usernamerefvalue = usernameref.elementAt(0);
    //                the spinner
                Dialog spinner = new InfiniteProgress().showInifiniteBlocking();
//        check if the values are empty and prevent from submitting
        if(districtpickvalue.equals("")||communitynamevalue.equals("")||beneficiarynamevalue.equals("")||sexpickvalue.equals("")||numberhouseholdvalue.equals("")||numberofsingleroomvalue.equals("")||numberofmalesvalue.equals("")||numberoffemalesvalue.equals("")||interventionpickvalue.equals("")||gpscordvalue.equals("")||meterpic.equals("")||pipepic.equals("")||commentFieldvalue.equals("")){
            spinner.dispose();
            Dialog.show("WARNING", "Please fill out all fields", "OK",null);
        }
        else{
//   posting all to an an api
        Map<String, String> applicantdata = new HashMap<String,String>();
                  applicantdata.put("districtpickvalue", districtpickvalue);
                  applicantdata.put("communitynamevalue", communitynamevalue);
                  applicantdata.put("beneficiarynamevalue", beneficiarynamevalue);
                  applicantdata.put("sexpickvalue", sexpickvalue);
                  applicantdata.put("numberhouseholdvalue", numberhouseholdvalue);
                  applicantdata.put("numberofsingleroomvalue", numberofsingleroomvalue);
                  applicantdata.put("numberofmalesvalue", numberofmalesvalue);
                  applicantdata.put("numberoffemalesvalue", numberoffemalesvalue);
                  applicantdata.put("interventionpickvalue", interventionpickvalue);
                  applicantdata.put("gpscordlatitude",Double.toString(gpscordlatitude));
                  applicantdata.put("gpscordlongitude",Double.toString(gpscordlongitude));
                  applicantdata.put("meterpicvalue", meterpicvalue);
                  applicantdata.put("pipepicvalue", pipepicvalue);
                  applicantdata.put("commentFieldvalue", commentFieldvalue);
                  applicantdata.put("usernamerefvalue", usernamerefvalue);
                  
                  final String result = Result.fromContent(applicantdata).toString(); 
                    String url = "http://139.162.226.4/api/public/index.php/api/officers/applicant";
        
                Response<Map> jsonData = Rest.post(url).contentType("application/json").body(result).acceptJson().getAsJsonMap();
                int jsonresponse = jsonData.getResponseCode();
                 if(jsonresponse == 200){
                     spinner.dispose();
                    Dialog.show("Success", "Data successfully saved", "OK",null);
                    padding.clearClientProperties();
                 }
                 if(jsonresponse == 404){
                    spinner.dispose();
                 Dialog.show("Error", "Ensure corrent input of data", "OK",null);
                 }
            }
//        end of else
        });
//         end of event listener
        padding.addComponent(submitButton);
        f.setLayout(new BorderLayout());
        f.addComponent(BorderLayout.CENTER, padding);
        f.show();
    }
                  
//    form for  changing password
public void showchangepass(){
        Form f = new Form("CHANGE PASSWORD");
//    for the side menu 
     Toolbar tb = f.getToolbar();
       Container topBar = BorderLayout.center(new Label("WELCOME OFFICER"));
        topBar.add(BorderLayout.SOUTH, new Label("MENU", "SidemenuTagline")); 
        topBar.setUIID("SideCommand");
        tb.addComponentToSideMenu(topBar);

        tb.addMaterialCommandToSideMenu("New", FontImage.MATERIAL_HOME, e -> {showdatalist();}); 
        tb.addMaterialCommandToSideMenu("Change Password", FontImage.MATERIAL_SETTINGS, e -> {showchangepass();});
//        getting the current page
//        final Form currentForm = Display.getInstance().getCurrent();
        
        tb.addMaterialCommandToSideMenu("Logout", FontImage.MATERIAL_HOURGLASS_FULL, e -> {showlogin();});
        
        Container padding = new Container();
        Style s = new Style();
        s.setPadding(0, 15, 5, 5);
        s.setPaddingUnit(new byte[]{
            Style.UNIT_TYPE_DIPS, 
            Style.UNIT_TYPE_DIPS,
            Style.UNIT_TYPE_DIPS,
            Style.UNIT_TYPE_DIPS
        });
      
        padding.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        padding.addComponent(new Label("Old Password"));
        TextField oldpasswordField = new TextField();
        padding.addComponent(oldpasswordField);
        
        padding.addComponent(new Label("New Password"));
        TextField newpasswordField = new TextField();
        padding.addComponent(newpasswordField);
        
        padding.addComponent(new Label("Retype Password"));
        TextField retypenewpasswordField = new TextField();
        padding.addComponent(retypenewpasswordField);
//        field contraints
        oldpasswordField.setConstraint(TextField.PASSWORD);
        newpasswordField.setConstraint(TextField.PASSWORD);
        retypenewpasswordField.setConstraint(TextField.PASSWORD);
              
        Button changeButton = new Button("CHANGE");
        changeButton.addActionListener((e)->{
            try {

                String oldpass = oldpasswordField.getText();
                String newpass = newpasswordField.getText();
                String retypenewpass = retypenewpasswordField.getText();
                if(oldpass.length()== 0 & newpass.length() == 0 & retypenewpass.length() == 0){
//                    show i input boxes are empty
                Dialog.show("MISMATCH", "Passwords do not match", "OK", null);
                }else{
//                    load page i any text is in them
                showlogin();
                }
            }
            catch (Exception ex) {
                Log.e(ex);
                Dialog.show("Change of password Failed", ex.getMessage(), "OK",null);
                return;
            }
        });
        padding.addComponent(changeButton);
        f.setLayout(new BorderLayout());
        f.addComponent(BorderLayout.CENTER, padding);
        f.show();
}

}
